# Introduction
Have you ever had to add a ton of WinCollect log sources to QRadar, but couldn't use the bulk add tool because of its arbitrary limitations? It's a pain in the ass, isn't it? I got frustrated on day and decided to write a little JavaScript to try to speed up the process. This is what came of that.  

# Security Considerations
FYI - to successfully run this script, and part of what makes it take less time than manually adding every single damn log source, is that you don't have to copy/paste the password each time, because it is stored in a variable in the bookmarklet. This password is probably to a domain admin account (yay for that!). So this is probably something you _**don't**_ want to store permanently in your bookmarks bar, especially if you use Firefox or Chrome syncing. **You have been warned!!!**  

# Usage
##Options
`unlikeIdentifier`  

* `False` (default) – Prompts the user for the name of the log source, and uses that as the log source identifier as well.   
* `True` – Prompts the user for the log source name first and then for the log source identifier. Useful, for example, if you need to use an IP as the identifier because the target does not have a DNS entry.    
    
`autoSave`
  
* `False` (default) – Skips the click event so you have the opportunity to review your settings before saving the new log source   
* `True` – Sends a click event to the Save Button without any further interaction, to speed the process of entering log sources. Enable it once you've dialed in your other settings   

## Variables  
`domain` – the AD Domain that the server resides in  
`username` – the username of the domain admin or service account you are using to remotely poll  
`password` – the password for the above account. Duh.  
`pollInterval` – interval in milliseconds that you'd like the server to poll the remote host  
`winCollectServer` – the name of the WinCollect server that you are using to do the remote polling. Typically in the format of `WinCollect @ hostname`  
`logSourceGroup` – the identifier of the log source group you'd like the new log source dropped into. Leave it empty if you want the log source to go into the default `Other` log source group, or inspect the element of the log source group checkbox with your browser dev tools to find the value you need to put in here  

## Setup
Got everything above all figured out? Good. Now run the three functions through your minifier of choice. I like the [Online JavaScript/CSS Compressor][1], but that's just me. Take those outputs and create three new bookmarks on your toolbar. Name the bookmarks something you'll remember, like `Set MS Security Event Log`, `Set WinCollect protocol`, and `Log Source @ WinCollectServer`, and paste the appropriate code into the appropriate bookmark. 

## Make the magic happen
Once you're all set up, log into your QRadar console, and get to where you can add a new log source. Once you're on the `Add new log source` page, you'll have five simple steps (if you've gone with all the defaults).  
  1. Click the first bookmarklet (`Set MS Security Event Log`)  
  2. Click the second bookmarklet (`Set WinCollect protocol`)  
  3. Click the third bookmarklet (`Log Source @ WinCollectServer`)  
  4. Paste the hostname of the new log source & hit `OK`  
  5. Review your settings and click the `Save` button  

Go ahead and check your `Other` log source group, and you'll see your freshly added WinCollect log source. Huzzah! Only took about 10 seconds right? sure beats having to copy/paste a hostname, domain, username, password, and set the options over and over and over and ov... okay you get the point.  

# Versions/Changelog
1.0 Initial publication   

    +-- SetLogSourceServerType.js  
    +-- SetLogSourceProtocolType.js  
    +-- SetupLogSource.js  

# Author
Craine Runton  
[@craine_runton][2]

# License
These project is licensed under [Creative Commons BY-NC-SA 4.0][3]

[1]: http://refresh-sf.com
[2]: https://www.twitter.com/craine_runton
[3]: http://creativecommons.org/licenses/by-nc-sa/4.0