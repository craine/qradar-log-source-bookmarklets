javascript:(
  function SetupLogSource(){
    /**************
    * The Options *
    **************/
    var unlikeIdentifier = FALSE,
    autoSave = FALSE; 
    
    /****************
    * The Variables *
    ****************/
    var logSourceHostname = window.prompt('Please enter the hostname of the Log Source you\'d like to add:');
    if (unlikeIdentifier) {
      var logSourceIdentifier = window.prompt('Please enter the identifier of the Log Source you\'d like to add:');
    } else {
      var logSourceIdentifier = logSourceHostname;
    };
    var domain = 'galactica.com',
      username = 'husker',
      password = 'apollo',
      pollInterval = '5000',
      wincollectServer = 'WinCollect @ WinCollectHostname',
      logSourceGroup = '100001';
  
    /************
    * The Magic *
    ************/
    $('input[name="deviceName"]').val(logSourceHostname);
    $('input[name="deviceIdentifier"]').val(logSourceIdentifier);
    $('input[name="value(Login_Domain)"]').val(domain);
    $('input[name="value(Login_Username)"]').val(username);
    $('input[name="value(Login_Password)"]').val(password);
    $('input[name="value(ConfirmPassword)"]').val(password);
    $('select[name="value(EventRateTuningProfile)"]').val('Typical Server');
    $('input[name="value(RemoteMachinePollInterval)"]').val(pollInterval);
    $('input[name="value(Log_System)"]').prop('checked',true);
    $('select[name="targetInternalDestinationId"]').val('2');
    $('select[name="value(WinCollectInstanceName)"]').val(wincollectServer);
    if (logSourceGroup !== '')  {
      $('input:checkbox[value='+logSourceGroup+']').prop('checked',true);
    };
    if (autoSave) {
      $('#saveButton').click();
    };
  }
)();